/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.model.importer;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.EcoreUtil;

import eu.paasage.camel.Application;
import eu.paasage.camel.CamelFactory;
import eu.paasage.camel.CamelModel;
import eu.paasage.camel.location.CloudLocation;
import eu.paasage.camel.location.Country;
import eu.paasage.camel.location.GeographicalRegion;
import eu.paasage.camel.location.LocationFactory;
import eu.paasage.camel.location.LocationModel;
import eu.paasage.camel.metric.Metric;
import eu.paasage.camel.metric.MetricModel;
import eu.paasage.camel.organisation.CloudProvider;
import eu.paasage.camel.organisation.DataCenter;
import eu.paasage.camel.organisation.Entity;
import eu.paasage.camel.organisation.Organisation;
import eu.paasage.camel.organisation.OrganisationModel;
import eu.paasage.camel.organisation.User;
import eu.paasage.camel.provider.ProviderModel;
import eu.paasage.camel.requirement.LocationRequirement;
import eu.paasage.camel.type.TypeFactory;
import eu.paasage.camel.type.TypeModel;
import eu.paasage.camel.unit.Dimensionless;
import eu.paasage.camel.unit.MonetaryUnit;
import eu.paasage.camel.unit.RequestUnit;
import eu.paasage.camel.unit.StorageUnit;
import eu.paasage.camel.unit.ThroughputUnit;
import eu.paasage.camel.unit.TimeIntervalUnit;
import eu.paasage.camel.unit.TransactionUnit;
import eu.paasage.camel.unit.Unit;
import eu.paasage.camel.unit.UnitFactory;
import eu.paasage.camel.unit.UnitModel;
import eu.paasage.camel.unit.UnitType;
import eu.paasage.mddb.cdo.client.CDOClient;
import eu.paasage.mddb.model.importer.location.LocationGenerator;
import eu.paasage.mddb.model.importer.security.SecurityPopulator;

public class ModelImporter {
	private boolean inCdo = false;
	private CDOClient cl = null;
	private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(CDOClient.class);
	private boolean hibernate = false;
	
	public ModelImporter(){
	}
	
	/* the constructor takes as input a boolean parameter indicating whether we store imported
	 * or generated information in the CDO repository. In case we do, then the respective
	 * CDO Repository resource folder structure is created.
	 */
	public ModelImporter(boolean inCdo){
		this.inCdo = inCdo;
		cl = new CDOClient("Administrator","0000");
		if (inCdo) createRepositoryStructure();
	}
	
	/* the constructor takes as input a boolean parameter indicating whether we store imported
	 * or generated information in the CDO repository. In case we do, then the respective
	 * CDO Repository resource folder structure is created.
	 */
	public ModelImporter(boolean inCdo, boolean hibernate){
		this.inCdo = inCdo;
		this.hibernate = hibernate;
		cl = new CDOClient("Administrator","0000");
		if (inCdo) createRepositoryStructure();
	}
	
	/* This method is used to import geolocations in the form of a location model
	 * into the CDO Repository or the file system. It takes as input two parameters. 
	 * The first parameter indicates the path to the file where a geolocation ontology
	 * is situated which is processed in order to create the location model. The second
	 * parameter is the path of the file to store the location model, in case that
	 * the inCdo boolean parameter has been set to false. The output of this method indicates
	 * whether the importing has been successful or not.
	 */
	public boolean importGeoLocationModels(String path, String outputPath){
		LocationGenerator lg = new LocationGenerator(path);
		LocationModel lm = lg.getLocationModel();
		if (lm != null){
			boolean stored = false;
			if (inCdo){
				boolean created = createCDOFolderStructure("locations",null);
				if (created) stored = cl.storeModel(lm, "locations/locations", true);
			}
			else{
				stored = cl.exportModel(lm, outputPath);
			}
			return stored;
		}
		return false;
	}
	
	/* This method just creates a specific CDO resource folder path and sub-path, 
	 * where both paths are given in the form of Strings as input parameters to
	 * this method. The output indicates whether the creation of the folder path
	 * and sub-path in the CDO Repository has been successful.
	 */
	private boolean createCDOFolderStructure(String path,String subPath){
		try{
			CDOTransaction trans = cl.openTransaction();
			trans.getOrCreateResourceFolder(path);
			if (subPath != null && !subPath.isEmpty()) trans.getOrCreateResourceFolder(path + "/" + subPath);
			trans.commit();
			trans.close();
			return true;
		}
		catch(Exception e){
			logger.error("Something went wrong while creating Root Folder for Providers in CDO Repository",e);
		}
		return false;
	}
	
	/* This method is used to import one or more provider models into the 
	 * CDO repository, where these models are stored in a specific directory
	 * whose path in the file system is given as input to this method. The
	 * output indicates whether the provider models have been successfully stored.
	 */
	public boolean importProviderModels(String dirPath){
		if (inCdo){
			File f = new File(dirPath);
			if (f.exists() && f.isDirectory()){
				boolean created = createCDOFolderStructure("/orgs","cps");
				if (created){
					File[] files = f.listFiles();
					for (File file: files){
						EObject model = cl.loadModel(file.getAbsolutePath());
						if (model instanceof CamelModel){
							CDOTransaction trans = cl.openTransaction();
							CamelModel cm = (CamelModel)model;
							EList<LocationModel> locModels = cm.getLocationModels();
							if (locModels != null && !locModels.isEmpty()){
								fixLocationModel(locModels.get(0),trans);
							}
							EList<OrganisationModel> oms = cm.getOrganisationModels();
							if (oms != null && !oms.isEmpty()){
								OrganisationModel om = oms.get(0);
								CloudProvider cp = om.getProvider();
								if (cp != null){
									String name = cp.getName().toLowerCase();
									boolean created2 = createCDOFolderStructure("/orgs/cps",name);
									CDOResource res = trans.getOrCreateResource("/orgs/cps/" + name + "/" + name);
									((CamelModel) model).getLocationModels().clear();
									res.getContents().add(model);
									try{
										trans.commit();
									}
									catch(Exception e){
										logger.error("Something went wrong while importing provider models",e);
									}
									trans.close();
									//cl.storeModel(model,"/orgs/cps/" + name + "/" + name,true);
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	/* This method is called in order to fix the locations in the requirement models 
	 * and data centers that have been stored in the CDO Repository according to
	 * a particular location model which is given as input along with the respective
	 * transaction under which this fixing takes place.
	 */
	private void fixLocationModel(LocationModel model, CDOTransaction trans){
		for (Country country: model.getCountries()){
			String name = country.getName();
			logger.debug("Processing country: " + name);
			Country corCountry = null;
			if (!hibernate)
				corCountry = trans.createQuery("sql", "select distinct c.* from CAMEL_LOCATION_COUNTRY c, CAMEL_LOCATION_COUNTRY_ALTERNATIVENAMES_LIST list where c.name like '%" + name + "%' or (list.cdo_source=c.cdo_id and list.cdo_value='" + name + "')").getResult(Country.class).get(0);
			else
				corCountry = trans.createQuery("hql", "select c from Country c where c.name like '%" + name + "%' or '" + name + "' member of c.alternativeNames").getResult(Country.class).get(0);
			Collection<Setting> references = EcoreUtil.UsageCrossReferencer.find(country, country.eResource());
			for (Setting setting : references) {
				  EObject source = setting.getEObject();
				  logger.debug("Source is: " + source);
				  if (source instanceof LocationRequirement){
					  LocationRequirement lr = (LocationRequirement)source;
					  lr.getLocations().remove(country);
					  lr.getLocations().add(corCountry);
				  }
				  else if (source instanceof DataCenter){
					  DataCenter dc = (DataCenter)source;
					  dc.setLocation(corCountry);
				  }
			}
		}
		for (GeographicalRegion region: model.getRegions()){
			String name = region.getName();
			logger.debug("Processing region: " + name);
			GeographicalRegion corRegion = null;
			if (!hibernate)
				corRegion = trans.createQuery("sql", "select distinct r.* from CAMEL_LOCATION_GEOGRAPHICALREGION r, CAMEL_LOCATION_GEOGRAPHICALREGION_ALTERNATIVENAMES_LIST list where r.name='" + name + "' or (list.cdo_source=r.cdo_id and list.cdo_value='" + name + "')").getResult(GeographicalRegion.class).get(0);
			else
				corRegion = trans.createQuery("hql", "select r from GeographicalRegion r where r.name='" + name + "' or '" + name + "' member of r.alternativeNames").getResult(GeographicalRegion.class).get(0);
			Collection<Setting> references = EcoreUtil.UsageCrossReferencer.find(region, region.eResource());
			for (Setting setting : references) {
				  EObject source = setting.getEObject();
				  logger.debug("Source is: " + source);
				  if (source instanceof LocationRequirement){
					  LocationRequirement lr = (LocationRequirement)source;
					  lr.getLocations().remove(region);
					  lr.getLocations().add(corRegion);
				  }
				  else if (source instanceof DataCenter){
					  DataCenter dc = (DataCenter)source;
					  dc.setLocation(corRegion);
				  }
			}
		}
	}
	
	/* This method is used for processing and storing in the CDO Repository a use case 
	 * model that is given as input. The output indicates whether the storage has been 
	 * successful.
	 */
	private boolean processUseCaseModel(EObject model){
		if (model instanceof CamelModel){
			CamelModel cm = (CamelModel)model;
			OrganisationModel om = cm.getOrganisationModels().get(0);
			String orgName = null;
			String providerName = null;
			Organisation org = om.getOrganisation();
			boolean stored = false;
			String orgPath = null;
			EcoreUtil.Copier copier = new EcoreUtil.Copier();
			User user = (User)copier.copy(om.getUsers().get(0));
			Application app = (Application)copier.copy(cm.getApplications().get(0));
			if (org != null){
				orgName = org.getName().toLowerCase();
				boolean created = createCDOFolderStructure("/orgs",orgName);
				if (created){
					for (User u: om.getUsers()) u.getRequirementModels().clear();
					stored = cl.storeModel(om, "/orgs/" + orgName + "/" + orgName, true);
				}
				orgPath = "/orgs/" + orgName;
			}
			else{
				CloudProvider cp = om.getProvider();
				providerName = cp.getName().toLowerCase();
				boolean created = createCDOFolderStructure("/orgs/cps",providerName);
				if (created){
					for (User u: om.getUsers()) u.getRequirementModels().clear();
					ProviderModel pm = cp.getProviderModel();
					if (pm != null){
						CamelModel newModel = CamelFactory.eINSTANCE.createCamelModel();
						newModel.setName("Provider " + providerName + " Camel Model");
						newModel.getOrganisationModels().add(om);
						newModel.getProviderModels().add(pm);
						stored = cl.storeModel(newModel, "/orgs/cps/" + providerName + "/" + providerName, true);
					}
					else{
						stored = cl.storeModel(om, "/orgs/cps/" + providerName + "/" + providerName, true);
					}
				}
				orgPath = "/orgs/cps/" + providerName;
			}
			if (stored){
				String userEmail = user.getEmail();
				createCDOFolderStructure(orgPath,userEmail);
				cm.getOrganisationModels().clear();
				cm.getProviderModels().clear();
				Entity ent = app.getOwner();
				if (ent instanceof User){
					User u = (User)ent;
					if (u.getEmail().equals(user.getEmail())){
						app.setOwner(null);
					}
				}
				String userPath = orgPath + "/" + userEmail;
				try{
					CDOTransaction trans = cl.openTransaction();
					CDOResource userRes = trans.getOrCreateResource(userPath + "/" + userEmail);
					for (LocationModel lm: cm.getLocationModels()) fixLocationModel(lm,trans);
					cm.getLocationModels().clear();
					userRes.getContents().add(cm);
					trans.commit();
					trans.close();
				}
				catch(Exception e){
					logger.error("Something went wrong while importing use case model / user-specific part",e);
				}
				if (stored){
					try{
						CDOTransaction trans = cl.openTransaction();
						OrganisationModel om2 = (OrganisationModel)trans.getOrCreateResource(orgPath + "/" + orgName).getContents().get(0);
						CamelModel cm2 = (CamelModel)trans.getOrCreateResource(userPath + "/" + userEmail).getContents().get(0);
						User u = null;
						if (!hibernate)
							u = trans.createQuery("sql", "select u.* from CAMEL_ORGANISATION_USER u where u.email='" + user.getEmail() + "'").getResult(User.class).get(0);
						else
							u = trans.createQuery("hql", "select u from User u where u.email='" + user.getEmail() + "'").getResult(User.class).get(0);
						u.getRequirementModels().addAll(cm2.getRequirementModels());
						for (Application a: cm2.getApplications()) a.setOwner(u);
						trans.commit();
						trans.close();
					}
					catch(Exception e){
						logger.error("Something went wrong while fixing cross-references between user and organisation models according to the model storage guidelines",e);
					}
					cl.exportModel(orgPath + "/" + orgName, OrganisationModel.class, "output/OrgModel.xmi");
					cl.exportModel(userPath + "/" + userEmail, CamelModel.class, "output/UserModel.xmi");
				}
			}
		}
		return false;
	}
	
	/* This method is used to import use case models. It is assumed that these models have been
	 * stored in a specific jar file which is inside the dependencies of this code. For
	 * the current time being, only the Scalarm use case model is imported but the code doing
	 * that is quite generic to accommodate also other use case models. It is expected that
	 * the signature of the method will be modified to include the resource paths to the use case
	 * models to import. The method returns as output the result of the importing in terms of success. 
	 */
	public boolean importUseCaseModels(){
		URL url = cl.getClass().getResource("/Scalarm.xmi");
		EObject model = cl.loadModel(url);
		boolean processed = processUseCaseModel(model);
		return processed;
	}
	
	/* This method just creates an empty type model and returns it as output.*/
	private EObject createEmptyTypeModel(){
		TypeModel tm = TypeFactory.eINSTANCE.createTypeModel();
		tm.setName("Camel_Type_Model");
		return tm;
	}
	
	/* This method is used to import cloud locations in the location model stored in the
	 * CDO Repository. This method will be modified and merged into the one of importing
	 * provider models as cloud locations are specific to a certain cloud provider and
	 * are not specified in a separate file/model!!!
	 * The method takes as input the path to the file system where the cloud location model
	 * is stored and returns as output a boolean value indicating whether the importing
	 * into the CDO Repository was successful or not. 
	 */
	private boolean importCloudLocations(String path){
		boolean ok = false;
		LocationModel lm = (LocationModel)cl.loadModel(path);
		EList<CloudLocation> cloudLocations = lm.getCloudLocations();
		CDOTransaction trans = cl.openTransaction();
		CDOResource res = trans.getOrCreateResource("locations/locations");
		LocationModel lm2 = (LocationModel)res.getContents().get(0);
		EList<CloudLocation> cloudLocations2 = lm2.getCloudLocations();
		DataCenter flexiant = null;
		DataCenter gwdg = null;
		if (!hibernate){
			flexiant = trans.createQuery("sql", "select dc.* from CAMEL_ORGANISATION_ORGANISATION cp, CAMEL_ORGANISATION_DATACENTER dc, CAMEL_ORGANISATION_ORGANISATIONMODEL om, CAMEL_ORGANISATION_ORGANISATIONMODEL_DATACENTRES_LIST list where cp.name='Flexiant' and list.cdo_source=om.cdo_id and list.cdo_value=dc.cdo_id and om.organisation=cp.cdo_id").getResult(DataCenter.class).get(0);
			gwdg = trans.createQuery("sql", "select dc.* from CAMEL_ORGANISATION_ORGANISATION cp, CAMEL_ORGANISATION_DATACENTER dc, CAMEL_ORGANISATION_ORGANISATIONMODEL om, CAMEL_ORGANISATION_ORGANISATIONMODEL_DATACENTRES_LIST list where cp.name='GWDG' and list.cdo_source=om.cdo_id and list.cdo_value=dc.cdo_id and om.organisation=cp.cdo_id").getResult(DataCenter.class).get(0);
		}
		else{
			flexiant = trans.createQuery("hql", "select dc from Organisation cp, Datacenter dc, OrganisationModel om where cp.name='Flexiant' and dc member of om.dataCentres and om.provider=cp").getResult(DataCenter.class).get(0);
			gwdg = trans.createQuery("hql", "select dc from Organisation cp, Datacenter dc, OrganisationModel om where cp.name='GWDG' and dc member of om.dataCentres and om.provider=cp").getResult(DataCenter.class).get(0);
		}
		for(CloudLocation cloc: cloudLocations){
			String id = cloc.getId();
			logger.debug("Checking CloudLocation: " + id);
			CloudLocation cloc2 = LocationFactory.eINSTANCE.createCloudLocation();
			cloc2.setId(cloc.getId());
			cloc2.setIsAssignable(cloc.isIsAssignable());
			EList<CloudLocation> subLocs = cloc2.getSubLocations();
			subLocs.addAll(cloc.getSubLocations());
			for(CloudLocation subLoc: subLocs) subLoc.setParent(cloc2);
			if (id.equals("VDC")){
				cloc2.setGeographicalRegion((GeographicalRegion)flexiant.getLocation());
			}
			else if (id.equals("One")){
				//System.out.println("Adding CloudLocation for GWDG");
				cloc2.setGeographicalRegion((GeographicalRegion)gwdg.getLocation());
			}
			cloudLocations2.add(cloc2);
		}
		try{
			trans.commit();
			ok = true;
		}
		catch(Exception e){
			logger.error("Something went wrong while importing cloud locations",e);
		}
		trans.close();
		return ok;
	}
	
	/* This method is used for creating a basic unit model which covers all possible units
	 * that can be created by the unit meta-model. This basic unit model is finally returned
	 * as output of this method. 
	 */
	private EObject createUnitModel(){
		UnitModel um = UnitFactory.eINSTANCE.createUnitModel();
		um.setName("Global_Unit_Model");
		EList<Unit> units = um.getUnits();
		//Create Units per Dimension
		/*!!!CoreUnit cu = UnitFactory.eINSTANCE.createCoreUnit();
		cu.setName("cores");
		cu.setUnit(UnitType.CORES);
		units.add(cu);*/
		Dimensionless dim = UnitFactory.eINSTANCE.createDimensionless();
		dim.setUnit(UnitType.PERCENTAGE);
		dim.setName("percentage");
		units.add(dim);
		MonetaryUnit euros = UnitFactory.eINSTANCE.createMonetaryUnit();
		euros.setName("euros");
		euros.setUnit(UnitType.EUROS);
		units.add(euros);
		MonetaryUnit dollars = UnitFactory.eINSTANCE.createMonetaryUnit();
		dollars.setName("dollars");
		dollars.setUnit(UnitType.DOLLARS);
		units.add(dollars);
		MonetaryUnit pounds = UnitFactory.eINSTANCE.createMonetaryUnit();
		pounds.setName("pounds");
		pounds.setUnit(UnitType.POUNDS);
		units.add(pounds);
		RequestUnit ru = UnitFactory.eINSTANCE.createRequestUnit();
		ru.setName("requests");
		ru.setUnit(UnitType.REQUESTS);
		units.add(ru);
		StorageUnit bytes = UnitFactory.eINSTANCE.createStorageUnit();
		bytes.setName("bytes");
		bytes.setUnit(UnitType.BYTES);
		units.add(bytes);
		StorageUnit kilobytes = UnitFactory.eINSTANCE.createStorageUnit();
		kilobytes.setName("kilobytes");
		kilobytes.setUnit(UnitType.KILOBYTES);
		units.add(kilobytes);
		StorageUnit megabytes = UnitFactory.eINSTANCE.createStorageUnit();
		megabytes.setName("megabytes");
		megabytes.setUnit(UnitType.MEGABYTES);
		units.add(megabytes);
		StorageUnit gigabytes = UnitFactory.eINSTANCE.createStorageUnit();
		gigabytes.setName("gigabytes");
		gigabytes.setUnit(UnitType.GIGABYTES);
		units.add(gigabytes);
		ThroughputUnit reqsPerSec = UnitFactory.eINSTANCE.createThroughputUnit();
		reqsPerSec.setName("requests_per_second");
		reqsPerSec.setUnit(UnitType.REQUESTS_PER_SECOND);
		units.add(reqsPerSec);
		ThroughputUnit transPerSec = UnitFactory.eINSTANCE.createThroughputUnit();
		transPerSec.setName("transactions_per_second");
		transPerSec.setUnit(UnitType.TRANSACTIONS_PER_SECOND);
		units.add(transPerSec);
		TimeIntervalUnit milliseconds = UnitFactory.eINSTANCE.createTimeIntervalUnit();
		milliseconds.setName("milliseconds");
		milliseconds.setUnit(UnitType.MILLISECONDS);
		units.add(milliseconds);
		TimeIntervalUnit seconds = UnitFactory.eINSTANCE.createTimeIntervalUnit();
		seconds.setName("seconds");
		seconds.setUnit(UnitType.SECONDS);
		units.add(seconds);
		TimeIntervalUnit minutes = UnitFactory.eINSTANCE.createTimeIntervalUnit();
		minutes.setName("minutes");
		minutes.setUnit(UnitType.MINUTES);
		units.add(minutes);
		TimeIntervalUnit hours = UnitFactory.eINSTANCE.createTimeIntervalUnit();
		hours.setName("hours");
		hours.setUnit(UnitType.HOURS);
		units.add(hours);
		TimeIntervalUnit days = UnitFactory.eINSTANCE.createTimeIntervalUnit();
		days.setName("days");
		days.setUnit(UnitType.DAYS);
		units.add(days);
		TimeIntervalUnit weeks = UnitFactory.eINSTANCE.createTimeIntervalUnit();
		weeks.setName("weeks");
		weeks.setUnit(UnitType.WEEKS);
		units.add(weeks);
		TimeIntervalUnit months = UnitFactory.eINSTANCE.createTimeIntervalUnit();
		months.setName("months");
		months.setUnit(UnitType.MONTHS);
		units.add(months);
		TransactionUnit trans = UnitFactory.eINSTANCE.createTransactionUnit();
		trans.setName("transactions");
		trans.setUnit(UnitType.TRANSACTIONS);
		units.add(trans);
		return um;
	}
	
	/* This method is used to create the CDO Repository resource folder structure 
	 * according to the model storage guidelines specified in the content of PaaSage WP2
	 */
	public void createRepositoryStructure(){
		//CDOClient cl = new CDOClient("Administrator","0000");
		CDOTransaction trans = cl.openTransaction();
		
		trans.getOrCreateResourceFolder("sec");
		trans.getOrCreateResourceFolder("locations");
		trans.getOrCreateResourceFolder("units");
		CDOResource res = trans.getOrCreateResource("units/units");
		res.getContents().add(createUnitModel());
		trans.getOrCreateResourceFolder("metrics");
		res = trans.getOrCreateResource("metrics/metrics");
		trans.getOrCreateResourceFolder("types");
		//Create an empty type model here
		res = trans.getOrCreateResource("types/types");
		res.getContents().add(createEmptyTypeModel());
		trans.getOrCreateResource("types/paasage_types");
		trans.getOrCreateResourceFolder("temporary");
		trans.getOrCreateResourceFolder("orgs");
		trans.getOrCreateResourceFolder("orgs/cps");
		
		try{
			trans.commit();
		}
		catch(Exception e){
			logger.error("Something went wrong while creating CDO Repository Structure",e);
		}
		cl.closeTransaction(trans);
		//cl.closeSession();
	}
	
	/* This method is used to generate a basic security model through processing a specific
	 * file whose path in the file system is provided as input to this method. The method takes
	 * also a second parameter as input which should be set when the security model generated
	 * has to be stored in the file system, thus pointing to the file path where the
	 * model will be stored. The method returns a boolean value indicating whether the
	 * importing in the CDO Repository or file system has been successful or not.
	 */
	public boolean importSecurityModel(String path, String resourcePath){
		if (inCdo) return SecurityPopulator.populate(path, cl, "sec/security", inCdo);
		else return SecurityPopulator.populate(path, cl, resourcePath, inCdo);
	}
	
	public boolean loadFixMetricModel(String filePath){
		MetricModel mm = (MetricModel)cl.loadModel(filePath);
		mm = EcoreUtil.copy(mm);
		EList<Unit> units = mm.getUnits();
		CDOTransaction trans = cl.openTransaction();
		List<Unit> replUnits = new ArrayList<Unit>();
		for (Unit u: units){
			if (!hibernate){
				List<Unit> uns = trans.createQuery("sql", "select u.* from CAMEL_UNIT_COREUNIT u where lower(u.name)='" + u.getName().toLowerCase() + "'").getResult(Unit.class);
				if (uns != null && !uns.isEmpty())
					replUnits.add(uns.get(0));
				else{
					uns = trans.createQuery("sql", "select u.* from CAMEL_UNIT_MONETARYUNIT u where lower(u.name)='" + u.getName().toLowerCase() + "'").getResult(Unit.class);
					if (uns != null && !uns.isEmpty())
						replUnits.add(uns.get(0));
					else{
						uns = trans.createQuery("sql", "select u.* from CAMEL_UNIT_REQUESTUNIT u where lower(u.name)='" + u.getName().toLowerCase() + "'").getResult(Unit.class);
						if (uns != null && !uns.isEmpty())
							replUnits.add(uns.get(0));
						else{
							uns = trans.createQuery("sql", "select u.* from CAMEL_UNIT_STORAGEUNIT u where lower(u.name)='" + u.getName().toLowerCase() + "'").getResult(Unit.class);
							if (uns != null && !uns.isEmpty())
								replUnits.add(uns.get(0));
							else{
								uns = trans.createQuery("sql", "select u.* from CAMEL_UNIT_THROUGHPUTUNIT u where lower(u.name)='" + u.getName().toLowerCase() + "'").getResult(Unit.class);
								if (uns != null && !uns.isEmpty())
									replUnits.add(uns.get(0));
								else{
									uns = trans.createQuery("sql", "select u.* from CAMEL_UNIT_TIMEINTERVALUNIT u where lower(u.name)='" + u.getName().toLowerCase() + "'").getResult(Unit.class);
									if (uns != null && !uns.isEmpty())
										replUnits.add(uns.get(0));
									else{
										uns = trans.createQuery("sql", "select u.* from CAMEL_UNIT_TRANSACTIONUNIT u where lower(u.name)='" + u.getName().toLowerCase() + "'").getResult(Unit.class);
										if (uns != null && !uns.isEmpty())
											replUnits.add(uns.get(0));
									}
								}
							}
						}
					}
				}
			}
			else{
				List<Unit> uns = trans.createQuery("hql", "select u from Unit u where lower(u.name)='" + u.getName().toLowerCase() + "'").getResult(Unit.class);
				if (uns != null && !uns.isEmpty())
					replUnits.add(uns.get(0));
			}
		}
		for (Metric m: mm.getMetrics()){
			Unit u = m.getUnit();
			if (u != null){
				int index = units.indexOf(u);
				m.setUnit(replUnits.get(index));
			}
		}
		mm.getUnits().clear();
		CDOResource res = trans.getOrCreateResource("metrics/metrics");
		res.getContents().add(mm);
		try{
			trans.commit();
			trans.close();
			return true;
		}
		catch(Exception e){
			logger.error("Something went wrong while attempting to store basic metric model",e);
		}
		return false;
	}
	
	/* This method should be called when the importing of all types of models has been finished
	 * as it is related to the closing of the CDOSession established through the internally
	 * exploited CDOClient.
	 */
	public void finish(){
		cl.closeSession();
	}
	
	public static void main(String[] args){
		int status = 0;
		if (args.length == 0){
			ModelImporter mi = new ModelImporter(true,false);
			mi.importGeoLocationModels("input/geopolitical.owl", "locations");
			mi.importProviderModels("input/providers");
			//mi.importCloudLocations("input/providers/CloudLocationModel.xmi");
			mi.loadFixMetricModel("input/Metric.xmi");
			mi.importSecurityModel("input/CSA_CCM_v3.0.1.xlsx","sec/security");
			mi.importUseCaseModels();
			mi.finish();
		}
		else{
			String method = args[0];
			ModelImporter mi = null;
			if (method.equals("importUseCaseModels")){
				mi = new ModelImporter(true);
				mi.importUseCaseModels();
				status = 2;
			}
			else if (method.equals("importProviderModels") || method.equals("importCloudLocations")){
				mi = new ModelImporter(true);
				if (args.length > 1){
					if (method.equals("importProviderModels"))
						mi.importProviderModels(args[1]);
					else if (method.equals("importCloudLocations")){
						mi.importCloudLocations(args[1]);
					}
					status = 2;
					if (args.length > 2){
						status = 1;
					}
				}
				else{
					status = 0;
				}
			}
			else if (method.equalsIgnoreCase("importGeoLocationModels") || method.equals("importSecurityModel")){
				boolean inCdo = Boolean.parseBoolean(args[1]);
				mi = new ModelImporter(inCdo);
				if (args.length >= 3){
					if (inCdo){
						if (method.equals("importGeoLocationModels"))
							mi.importGeoLocationModels(args[2], null);
						else if (method.equals("importSecurityModel"))
							mi.importSecurityModel(args[2], null);
						status = 2;
					}
					else{
						if (args.length >= 4){
							if (method.equals("importGeoLocationModels"))
								mi.importGeoLocationModels(args[2], args[3]);
							else if (method.equals("importSecurityModel"))
								mi.importSecurityModel(args[2], args[3]);
							status = 2;
						}
						else{
							status = 0;
						}
					}
					if (args.length > 4){
						status = 1;
					}
				}
				else{
					status = 0;
				}
			}
			else{
				logger.info("No method with the respective name: " + method + " exists or it is not readable to call it in a command line manner");
				status = 2;
			}
			if (status == 1){
				logger.info("More input arguments than needed have been provided. The rest will be ignored");
			}
			else if (status == 0){
				logger.error("Less input arguments than needed have been provided for method: " + method);
			}
			else{
				mi.finish();
			}
		}
		System.exit(0);
	}
}
