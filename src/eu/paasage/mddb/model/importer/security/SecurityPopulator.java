/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.model.importer.security;

import java.io.FileInputStream;
import java.util.Hashtable;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.emf.common.util.EList;

import eu.paasage.camel.CamelModel;
import eu.paasage.camel.unit.TimeIntervalUnit;
import eu.paasage.camel.security.SecurityDomain;
import eu.paasage.camel.security.SecurityControl;
import eu.paasage.camel.security.SecurityFactory;
import eu.paasage.camel.security.SecurityModel;
import eu.paasage.mddb.cdo.client.CDOClient;

public class SecurityPopulator {
	private static TimeIntervalUnit secs = null;
	private static CamelModel cm = null;
	private static Hashtable<String,String> ccm2ccm = new Hashtable<String,String>();
	private static Hashtable<String,SecurityControl> secContrIds = new Hashtable<String,SecurityControl>();
	private static Hashtable<String,SecurityDomain> secDomIds = new Hashtable<String,SecurityDomain>();
	private static final double threshold = 0.6;
	
	private static String nameToId(String name){
		String id = "";
		char[] chars = name.toCharArray();
		for (char c: chars){
			if (Character.isUpperCase(c)) id += c;
		}
		return id;
	}
	
	public static boolean populate(String path, CDOClient cl, String resourcePath, boolean inCdo){
		boolean ok = false;
		int secControlNum = 0;
		SecurityModel sm = null;
		//EList<SecurityModel> secModels = null;
		try{
		   FileInputStream file = new FileInputStream(path);
	    
		   //Get the workbook instance for XLS file
		   XSSFWorkbook workbook = new XSSFWorkbook(file);
		
		   //Get first sheet from the workbook
		   XSSFSheet sheet = workbook.getSheetAt(0);
		    
		   //Iterate through each rows from first sheet
		   Iterator<Row> rowIterator = sheet.iterator();
		   //Ignore three first rows
		   if (rowIterator.hasNext()){
			   for (int i = 0; i < 4; i++) rowIterator.next();
		   }
		   /*cm = CamelFactory.eINSTANCE.createCamelModel();
		   cm.setName("Security Camel Model");
		   secModels = cm.getSecurityModels();
		   EList<Unit> units = cm.getUnits();
		   secs = UnitFactory.eINSTANCE.createTimeIntervalUnit();
		   //u.setDimensionType(UnitDimensionType.UNITLESS);
		   secs.setUnit(UnitType.SECONDS);
		   secs.setName("seconds");
		   units.add(secs);*/
		   
		   sm = SecurityFactory.eINSTANCE.createSecurityModel();
		   sm.setName("Basic_Security_Model");
		   EList<SecurityControl> secControls = sm.getSecurityControls();
		   //Process remaining rows
		   int subDomainIndex = 0;
		   while (rowIterator.hasNext()){
			   Row row = rowIterator.next();
		       Cell cell = row.getCell(0);
		       if (cell == null) break;
		       String domain = cell.getStringCellValue();
		       if (domain == null || domain.trim().equals("")) break;
		       cell = row.getCell(1);
		       String id = cell.getStringCellValue().replace("-", "_");
		       String domainId = id.substring(0,id.indexOf("_"));
		       cell = row.getCell(2);
		       String description = cell.getStringCellValue();
		       SecurityControl sc = SecurityFactory.eINSTANCE.createSecurityControl();
		       domain = domain.replace("\n", " - ");
		       int index = domain.indexOf(" -");
		       String topDomainName = domain.substring(0,index);
		       String domainName = domain.substring(index + 3);
		       System.out.println("Top domain is: " + topDomainName);
		       
		       SecurityDomain sd = null;
		       sd = secDomIds.get(domainId);
		       if (sd == null){
		    	   sd = SecurityFactory.eINSTANCE.createSecurityDomain();
		    	   sd.setName(topDomainName);
		    	   sd.setId(domainId);
		    	   secDomIds.put(domainId, sd);
		    	   sm.getSecurityDomains().add(sd);
		    	   subDomainIndex = 0;
		       }
		       sc.setDomain(sd);
		       
		       SecurityDomain subDomain = SecurityFactory.eINSTANCE.createSecurityDomain();
		       subDomain.setName(domainName);
		       subDomain.setId(domainId + "_" + (subDomainIndex++) + "_" + nameToId(domainName));
		       sc.setName(id);
		       sc.setSubDomain(subDomain);
		       sm.getSecurityDomains().add(subDomain);
		       
		       cell = row.getCell(22);
		       String prevId = cell.getStringCellValue();
		       ccm2ccm.put(prevId, id);
		       //sc.setSpecification(description.substring(0,description.length() > 450? 450: description.length()));
		       sc.setSpecification(description);
		       System.out.println("Got security control with id: " + id + " domain: " + domain + " specification: " + description + " prevID: " + prevId);
		       
		       secControls.add(sc);
		       secControlNum++;
		       secContrIds.put(id,sc);
		       //mapSecControlToMetric(sc);
		   }
		}
		catch(Exception e){
			e.printStackTrace();
		}
		   System.out.println("Number of Security Controls is: " + secControlNum + " number of top domains is: " + secDomIds.size());
		   //secModels.add(sm);
		   //cl.exportModel(sm, "output/CCM.xmi");
		   if (inCdo)
			   ok = cl.storeModel(sm, resourcePath, true);
		   else
			   ok = cl.exportModel(sm, resourcePath);
		   return ok;
	}
}
