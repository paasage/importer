/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.model.importer.location;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

import eu.paasage.camel.location.GeographicalRegion;
import eu.paasage.camel.location.Country;
import eu.paasage.camel.location.LocationFactory;
import eu.paasage.camel.location.LocationModel;

public class LocationGenerator {
	private String path;
	private LocationModel lm = null;
	private boolean alterNames = false;
	
	public LocationGenerator(String path){
		this.path = path;
		loadLocationModel();
	}
	
	public LocationModel getLocationModel(){
		return lm;
	}
	
	private void loadLocationModel(){
		OntModel base = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
		try{
			Model model = base.read(new FileInputStream(path), null);
			if (model != null){
				String NS = "http://aims.fao.org/aos/geopolitical.owl#";
				Resource region = base.getResource(NS + "geographical_region");
				Resource selfGoverning = base.getResource(NS + "self_governing");
				Hashtable<String,GeographicalRegion> uriToGeographicalRegion = new Hashtable<String,GeographicalRegion>();
				Hashtable<String,List<Country>> regionToCountry = new Hashtable<String,List<Country>>();
				//Hashtable<String,GeographicalRegion> nameToGeographicalRegion = new Hashtable<String,GeographicalRegion>();
				ExtendedIterator<Individual> regionIt = base.listIndividuals(region);
				Property name = base.getProperty(NS + "nameOfficialEN");
				Property officialFrName = base.getProperty(NS + "nameOfficialFR");
				Property officialItName = base.getProperty(NS + "nameOfficialIT");
				Property officialEsName = base.getProperty(NS + "nameOfficialES");
				Property nameShort = base.getProperty(NS + "nameShortEN");
				Property nameShortFr = base.getProperty(NS + "nameShortFR");
				Property nameShortIt = base.getProperty(NS + "nameShortIT");
				Property nameShortEs = base.getProperty(NS + "nameShortES");
				Property inGroup = base.getProperty(NS + "isInGroup");
				Property code = base.getProperty(NS + "codeISO2");
				if (regionIt.hasNext()){
					lm = LocationFactory.eINSTANCE.createLocationModel();
					lm.setName("Locations");
					System.out.println("Got regions: " + regionIt);
					List<Individual> regions = regionIt.toList();
					for (Individual ind: regions){
						//GeographicalRegion creation plus official name handling
						Statement st = ind.getProperty(name);
						GeographicalRegion cn = LocationFactory.eINSTANCE.createGeographicalRegion();
						String officialName = st.getString();
						/*if (officialName.equals("Americas")) officialName = "America";
						else if (officialName.equals("south-eastern Asia")) officialName = "south-Eastern Asia";*/
						cn.setName(officialName);
						st = ind.getProperty(code);
						if (st == null){
							cn.setId(officialName.replace(" ", "_").replace("-","_"));
						}
						else{
							cn.setId(st.getString());
						}
						EList<String> alternativeNames = cn.getAlternativeNames();
						st = ind.getProperty(officialFrName);
						alternativeNames.add(st.getString());
						st = ind.getProperty(officialItName);
						alternativeNames.add(st.getString());
						st = ind.getProperty(officialEsName);
						alternativeNames.add(st.getString());
						
						// Short name handling
						st = ind.getProperty(nameShort);
						alternativeNames.add(st.getString());
						st = ind.getProperty(nameShortFr);
						alternativeNames.add(st.getString());
						st = ind.getProperty(nameShortIt);
						alternativeNames.add(st.getString());
						st = ind.getProperty(nameShortEs);
						alternativeNames.add(st.getString());
						
						//Updating of location model and hash tables
						lm.getRegions().add(cn);
						uriToGeographicalRegion.put(ind.getURI(), cn);
					}
					ExtendedIterator<Individual> countryIt = base.listIndividuals(selfGoverning);
					List<Individual> countries = countryIt.toList();
					for (Individual ind: countries){
						//Country creation plus official name handling
						Statement st = ind.getProperty(name);
						if (st == null) continue;
						Country cn = LocationFactory.eINSTANCE.createCountry();
						String officialName = st.getString();
						cn.setName(officialName);
						EList<String> alternativeNames = cn.getAlternativeNames();
						System.out.println("Processing country: " + officialName);
						st = ind.getProperty(officialFrName);
						if (st == null) continue;
						alternativeNames.add(st.getString());
						st = ind.getProperty(officialItName);
						if (st == null) continue;
						alternativeNames.add(st.getString());
						st = ind.getProperty(officialEsName);
						alternativeNames.add(st.getString());
						
						// Short name handling
						st = ind.getProperty(nameShort);
						alternativeNames.add(st.getString());
						st = ind.getProperty(nameShortFr);
						alternativeNames.add(st.getString());
						st = ind.getProperty(nameShortIt);
						alternativeNames.add(st.getString());
						st = ind.getProperty(nameShortEs);
						alternativeNames.add(st.getString());
						
						//ISO Code to short name
						st = ind.getProperty(code);
						cn.setId(st.getString());
						
						//Updating of location model and mapping to continents
						lm.getCountries().add(cn);
						StmtIterator it = ind.listProperties(inGroup);
						while (it.hasNext()){
							st = it.next();
							String URI = st.getObject().asResource().getURI();
							GeographicalRegion cont = uriToGeographicalRegion.get(URI);
							if (cont != null){
								cn.getParentRegions().add(cont);
								String regionName = cont.getName();
								List<Country> childCountries = regionToCountry.get(regionName);
								if (childCountries == null) childCountries = new ArrayList<Country>();
								childCountries.add(cn);
								regionToCountry.put(regionName, childCountries);
							}
						}
					}
					EList<GeographicalRegion> geoRegions = lm.getRegions();
					for (int i = 0; i < regions.size() - 1; i++){
						for (int j = i+1; j < regions.size(); j++){
							GeographicalRegion region1 = geoRegions.get(i);
							GeographicalRegion region2 = geoRegions.get(j);
							List<Country> countries1 = regionToCountry.get(region1.getName());
							List<Country> countries2 = regionToCountry.get(region2.getName());
							if (countries1.containsAll(countries2)) region2.getParentRegions().add(region1);
							else if (countries2.containsAll(countries1)) region1.getParentRegions().add(region2);
						}
					}
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
